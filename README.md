# Estendendo o Task Bench utilizando OmpCluster + CUDA

O objetivo desse repositório é armazenar o código fonte da apresentação do trabalho realizado em um projeto de Iniciação Científica. O projeto desenvolvido, foi a elaboração da implementação de uma versão do Task Bench, combinando os modelos de programação paralela e distribuida OmpCluster e CUDA.

Essa apresentação, consiste em um pôster virtual, apresentado no Workshop de Teses, Dissertações e Trabalhos de Iniciação Científica (WTD) 2022. O WTD é um evento promovido pelo Instituto de Computação (IC) da Universidade Estadual de Campinas (Unicamp). O WTD se encontra na sua XVII edição e foi realizado no Instituto de Computação da Unicamp, nos dias 12 e 13 de Dezembro de 2022.

[Pôster Virtual](https://cl3t0.gitlab.io/taskbench-ompc-cuda-wtd-2022/)