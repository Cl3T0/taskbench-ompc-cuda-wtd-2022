---
title: "Task Bench OMPC + CUDA"
date: 2022-12-11T17:38:49+05:30
type: page
showTableOfContents: true
---

# Estendendo o Task Bench utilizando OmpCluster + CUDA

Jhonatan Cléto, j256444@dac.unicamp.br

Hervé Yviquel, hyviquel@unicamp.br

Márcio Machado Pereira, m780681@dac.unicamp.br

## Contexto e Motivação

Os desafios da computação paralela e distribuída levaram a uma ampla variedade de propostas de modelos de programação, linguagens e sistemas runtime. Embora esses sistemas estejam bem documentados na literatura, avaliações de desempenho abrangentes e comparativas ainda são difíceis de encontrar.

Uma das metodologias mais comuns utilizadas para comparar algoritmos/softwares, é a comparação deles por meio de um benchmarking, no qual eles são executados em um mesmo ambiente, executando o mesmo conjunto de tarefas. Então, algumas métricas, por exemplo tempo de execução e uso de memória, são utilizadas para comparar o desempenho desses softwares.

Neste trabalho desenvolvemos um *benchmark* por meio do *framework* Task Bench para execução de *benchmarking* em *clusters* heterogêneos (CPUs+GPUs), utilizando o runtime OmpCluster e a API CUDA.

#### Task Bench

O Task Bench [1] é um framework para *benchmarking* parametrizado utilizado para comparar modelos de programação focados em paralelismo de tarefas. Ele fornece uma estrutura base para a construção de *benchmarks* em diversos sistemas e modelos de programação paralela e distribuída. O *Framework* utiliza uma abstração em *Grafo de Tarefas* para representar a estrutura de dependências de dados entre as tarefas.

No grafo, as tarefas são os vértices e as dependências entre as tarefas são as arestas. Segundo os autores do *benchmark*, a maioria das aplicações podem ser modeladas como um conjunto de tarefas com dependências entre si. Desse modo, o Task Bench consegue simular uma variedade de aplicações a partir de um conjunto de tipos de dependências comumente vistas em aplicações reais. Exemplos de grafos configuráveis no Task Bench são vistos na Figura 1.

![Task Bench Dependencies](/taskbench-ompc-cuda-wtd-2022/images/presentation/task-bench-dependencies.svg "Dependencias configuraveis no Task Bench")



*Figura 1: Exemplos de Grafos de Tarefas Configuraveis no Task Bench.*

#### OmpCluster

O OmpCluster (OMPC) [2] é um modelo de programação para *clusters* HPC baseado em paralelismo de tarefas. Ele estende a API do OpenMP (Open Multi-Processing) [6], permitindo o offloading de tarefas científicas complexas nos nós do cluster HPC de maneira transparente e equilibrada.

#### CUDA

CUDA [3] é um modelo de programação paralela que faz uso do poder computacional das GPUs NVIDIA para resolver muitos problemas complexos de maneira mais eficiente do que em uma CPU.

## Objetivo

O objetivo do desenvolvimento desta nova implementação do *benchmark* combinando OMPC e CUDA, é avaliar o desempenho do OMPC, quando combinado com outras APIs/Frameworks de programação paralela. Já que uma das premissas do runtime é permitir a extensão das capacidades do OpenMP utilizando por exemplo offloading para GPUs com CUDA.

## Métodos

Uma das estruturas básicas do Task Bench é o *kernel*, ele abstrai o tipo da tarefa que é executada nos nós do grafo durante o *benchmark* e as configurações relacionadas a essas tarefas.

Para o desenvolvimento da nova implementação do Task Bench [4], utilizamos como base a implementação já existente do *benchmark* para o OMPC e uma extensão do código base do framework que permite a execução de *kernels* em GPUs utilizando CUDA. Além dessa infraestrutura base, modificamos uma implementação preexistente [7] do Task Bench, que utiliza MPI e CUDA, que é utilizada para comparação de performance com a nossa nova implementação.

As novas estruturas trazidas ao Task Bench para permitir a execução de *kernels* CUDA, incluem a adição de parâmetros ao Task Graph relacionados a informações necessárias para a execução dos *kernels* CUDA.

Durante o desenvolvimento da versão OMPC+CUDA do Task Bench notamos uma limitação do OMPC que causa uma queda no desempenho dos *benchmarks* de *kernels* CUDA. Pela forma como a base CUDA do Task Bench foi projetada, é necessário alocar previamente alguns buffers requeridos durante a execução das tarefas. Contudo, esses buffers não conseguem ser gerenciados pelo OMPC. Assim, para conseguir contornar essa limitação, a implementação do OMPC+CUDA, aloca esses buffers para cada tarefa que será executada no *benchmarking* o que causa um *overhead* em cada execução de tarefa, impactando o desempenho do *benchmark*.

Para solucionar esse problema, estendemos o gerenciamento de memória do OMPC, utilizando sistema de memória unificada do CUDA, que permite o uso integrado da memória principal (RAM) com a memória da GPU. O uso desse sistema traz impactos ao OMPC, devido ao gerenciamento da migração de páginas entre CPU e GPU. Contudo, essa é uma abordagem interessante devido ao baixo custo de implementação, servindo como uma prova de conceito do que é possível fazer para melhorar a integração do OMPC com a API CUDA.

Utilizando essa abordagem, desenvolvemos uma segunda implementação do Task Bench OMPC+CUDA, se aproveitando do gerenciamento de memória do OMPC. Nessa nova versão conseguimos reduzir o número de alocações dos buffers, usados nas tarefas, ao número de nós que executam o *benchmarking* e como consequência, conseguimos reduzir o *overhead* causado pela alocação desses buffers no tempo de execução do *benchmarking*. 

## Resultados

Objetivando avaliar o desempenho das implementações OMPC+CUDA do Task Bench, executamos algumas configurações do *benchmark*, comparando-as com a implementação MPI+CUDA.

O *benchmarking* foi executado no Cluster Santos Dumont, utilizando 5 nós do cluster equipados com duas CPUs Intel Xeon Cascade Lake Gold 6252 @2.10GHz acompanhado de 384 GB de RAM e quatro GPUs Nvidia Volta V100 com 32 GB de VRAM. Em todos os benchmarks, os 5 nós foram utilizados. Os compiladores utilizados foram o Clang 14.0 e o nvcc 11.4. Foram utilizados ainda, o OmpCluster na versão 14.9.0 e o MPICH na versão 3.4.2.

As configurações do Grafo de Tarefa e dependências utilizadas nos *benchmarks* são vistos da Tabela 1 e alguns dos resultados obtidos são apresentados na Tabela 2.

| Configuração | Valor utilizado |
| -- | -- |
|  Dimensões do Grafo    |  4x32 |
|  Iterações             |  10K, 100K, 1M, 10M, 100M |
|  Depêndencias          |  trivial, stencil-1d, fft, tree |
|  Kernel                |  cuda_compute_bound |
|  Operações / Iteração  |  16  |

*Tabela 1: Configurações adotadas nos benchmarkings.*


|  Benchmark  |  MPI+CUDA  |  OMPC+CUDA (1)  |  OMPC+CUDA+GMU (2) | Speedup (1)| Speedup (2)|
| -- | -- | -- | -- | -- | -- |
| Trivial-10M   | 13.90s | 17.88s | 17.19s | 0.78x | 0.81x |
| Stencil1D-10M | 14.82s | 21.59s | 18.45s | 0.69x | 0.80x |
| FFT-10M       | 14.72s | 18.89s | 17.99s | 0.78x | 0.82x |
| Tree-10M      | 14.47s | 62.73s | 56.01s | 0.23x | 0.26x |

*Tabela 2: Resultados obtidos nos benchmarks, os valores indicam tempo de execução em segundos.*

Os resultados apresentados na Tabela 2, mostram que em todos os *benchmarks* executados, o tempo de execução da versão OMPC+CUDA foi maior comparado a versão MPI+CUDA, como visto na Tabela 2. Um dos motivos da diferença de desempenho se deve ao *overhead* do OMPC, necessário para fazer o gerenciamento de memória e distribuição de tarefas entre os nós, recursos que não existem na versão MPI+CUDA. Observamos ainda, que a versão que utiliza gerenciamento de memória unificada apresenta vantagens em relação a versão sem essa função, elucidando os ganhos provenientes das otimizações aplicadas.

## Conclusão

Neste trabalho, implementamos uma nova versão do Task Bench utilizando OMPC e CUDA, permitindo avaliar o desempenho do OMPC, quando ele trabalha em conjunto com outra API de programação paralela. Avaliamos essa nova versão em relação a implementação similar. A partir desse comparativo, encontramos limitações no OMPC, relacionadas à interação com outras APIs.

Para eliminar essas limitações e estender as capacidades do OMPC, utilizamos o sistema de memória unificada do CUDA, tornando o OMPC capaz de gerenciar buffers tanto na memória principal quanto na memória das GPUs em um nó do cluster.

Como resultados, observamos ganhos de performance utilizando o gerenciamento de memória unificada. No entanto, o desempenho dessa implementação ainda é inferior à implementação MPI+CUDA, indicando margens para melhorias em nossa implementação. 

Futuramente, planejamos otimizar o uso do gerenciamento de memória unificada, reduzindo o número de migrações de páginas entre CPU e GPU, aprimorar o *kernel* CUDA para reduzir a latência na execução das tarefas e fazer uso de todas as GPUs em um nó para a execução das tarefas a ele designadas.

Com o desenvolvimento dessa implementação, conseguimos obter vários insights sobre o comportamento do OMPC quando ele trabalha em conjunto com outros modelos de programação. Concluímos que, a integração entre o OmpCluster e CUDA é viável e bastante promissora, pois possibilita acelerar a execução de aplicações científicas em clusters HPC heterogêneos.

## Referências

[1]: Elliott Slaughter et al. “Task bench: A parameterized benchmark for evaluating parallel runtime performance”. Em: SC20: International Conference for High Performance Computing, Networking, Storage and Analysis. IEEE. 2020, pp. 1–15.

[2]: Hervé Yviquel et al. “The OpenMP Cluster Programming Model”. Em: 51st International
Conference on Parallel Processing Workshop Proceedings (ICPP Workshops 22) (2022).

[3]: Nvidia. CUDA Toolkit. https://docs.nvidia.com/cuda/cuda-toolkit-release-notes/index.html. Acesso: 07-12-2022.

[4]: Fork do repositório do Task Bench do OmpCluster. https://gitlab.com/ompcluster/task-bench. Acesso: 07-12-2022.

[5]: OmpCluster. https://ompcluster.gitlab.io. Acesso: 07-12-2022.

[6]: OpenMP. The OpenMP API specification for parallel programming. https://www.openmp.org. Acesso: 07-12-2022.

[7]: Repositório Oficial do Task Bench. https://github.com/StanfordLegion/task-bench.
Acesso: 07-12-2022.