---
title: "Task Bench OMPC + CUDA"
date: 2022-12-11T17:38:49+05:30
type: page
---

![WTD Logo](images/wtd2022.png "Logo WTD")

# Estendendo o Task Bench utilizando OmpCluster + CUDA

Jhonatan Cléto, j256444@dac.unicamp.br

Hervé Yviquel, hyviquel@unicamp.br

Márcio Machado Pereira, m780681@dac.unicamp.br

## Resumo

Neste Trabalho, desenvolvemos um *benchmark* utilizando *framework* Task Bench. Essa implementação, objetiva atender algumas das necessidades do time de desenvolvimento OmpCluster, um runtime para construção de aplicações paralelas em *clusters* HPC. O *benchmark* foi desenvolvida utilizando o OmpCluster e a API CUDA, na linguagem C++. Através de comparações com outra versão do *benchmark*, notamos limitações no OmpCluster, relacionadas a interação com outras APIs de programação paralela como CUDA. Para contornar essas limitações, estendemos as capacidades do OmpCluster, otimizando o benchmark elaborado.

[Pôster Virtual](https://cl3t0.gitlab.io/taskbench-ompc-cuda-wtd-2022/presentation/poster/)

## Conversa Virtual

- Data: Segunda-feira, 12 de dezembro de 2022 · 15:00 até 16:00
- Link do Meet: [Conversa Task Bench OMPC+CUDA](https://meet.google.com/mye-dnvk-kmq)